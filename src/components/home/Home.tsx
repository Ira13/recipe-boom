import React from 'react';
import './Home.scss';
import RecipeList from "../recipes-list/RecipeList";
import { useSelector } from "react-redux";
import { AppState } from "../../store/app.reducer";
import Loader from "react-loader-spinner";
import Categories from "../categories/Categories";

function Home() {
  const isLoading: boolean = useSelector((state: AppState) => state.recipes.isLoading);

  return (
    <div className='home-container'>
      <Categories></Categories>
      {isLoading
      ? <div className="spinner">
          <Loader type="ThreeDots" color="#facf5e" height={200}></Loader>
        </div>
      : <RecipeList/>}
    </div>
  );
}

export default Home;
