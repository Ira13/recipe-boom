import React, {useState} from "react";
import "./Categories.scss";
import {CategoriesMap, Category} from "./categories.config";
import {IRecipeSearchOptions} from "../../store/recipe.models";
import {useDispatch} from "react-redux";
import {
	FetchRandomList,
	FetchRecipesList,
	SetRecipesListSearchOptions
} from "../../store/recipe-list/recipe-list.actions";

export default function Categories() {

	const categories = CategoriesMap;
	const [selectedCategory, setSelectedCategory]: [Category, Function] = useState(Category.All);
	const dispatch = useDispatch();

	const getRecipes = (category: Category) => {
		setSelectedCategory(category);
		const request = CategoriesMap[category] as Partial<IRecipeSearchOptions>;
		if (category === Category.All) {
			dispatch(FetchRandomList());
		} else {
			dispatch(SetRecipesListSearchOptions(request));
			dispatch(FetchRecipesList());
		}
	}

	return (
		<div className="wrapper">
			{
				Object.keys(categories).map((category) =>
					<div
						key={category}
						className={(category === selectedCategory && "category active") || "category"}
						onClick={() => {getRecipes(category as Category)}}
					>
						{category}
					</div>
				)
			}
		</div>
	);
}
