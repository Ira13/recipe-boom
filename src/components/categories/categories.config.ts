export enum Category {
	All = 'All',
	VeryHealthy = 'Healthy',
	Vegetarian = 'Vegetarian',
	Desserts = 'Desserts',
	Italian = 'Italian',
	American = 'American',
	Chinese = 'Chinese',
}

export const CategoriesMap: {[key in Category]: {[key: string]: string | string[]}} = {
	[Category.All]: {},
	[Category.VeryHealthy]: {query: 'healthy'},
	[Category.Vegetarian]: {diet: 'vegetarian'},
	[Category.Desserts]: {query: 'desserts'},
	[Category.Italian]: {cuisine: 'italian'},
	[Category.American]: {cuisine: 'american'},
	[Category.Chinese]: {cuisine: 'chinese'},
}
