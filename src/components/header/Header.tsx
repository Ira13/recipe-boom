import React, {useEffect, useState} from "react";
import "./Header.scss";
import useDebounce from "../../utils/debounce-hook";
import {useDispatch} from "react-redux";
import {
	FetchRandomList,
	FetchRecipesList,
	UpdateRecipesListSearchOptions
} from "../../store/recipe-list/recipe-list.actions";
import {useLocation, Link} from 'react-router-dom';

export default function Header(props: any) {

	const [searchString, setSearchString]: [string, Function] = useState('');

	const debouncedSearchString = useDebounce(searchString, 300);
	const dispatch = useDispatch();

	const location = useLocation();
	useEffect(() => {
		dispatch(FetchRandomList());
	}, [])

	useEffect(
		() => {
			if (location.pathname !== '/') return;
			if (debouncedSearchString) {
				dispatch(UpdateRecipesListSearchOptions({query: debouncedSearchString}))
				dispatch(FetchRecipesList())
			} else {
				dispatch(FetchRandomList());
			}
		},
		[debouncedSearchString, dispatch]
	);

	return (
		<div className="header">
			<Link to="/">
				<img className="logo" alt="logo" src="/assets/logo.png"/>
			</Link>
			<div className="header-controls">
				{
					location.pathname === '/' &&
					<div className="input-with-icon search-input">
					<span className="material-icons input-icon">
											search
					</span>
						<input
							placeholder="Find your perfect recipe"
							className="search-input custom-input"
							value={searchString}
							onChange={e => setSearchString(e.target.value)}
						/>

					</div>
				}
				<button className="primary-btn" onClick={props.addNewRecipe}>Add new recipe</button>
			</div>

		</div>
	);
}
