import React, {useEffect} from "react";
import "./ViewRecipe.scss";
import {useDispatch, useSelector} from "react-redux";
import {GetRecipeDetailsById} from "../../store/recipe-details/recipe-details.actions";
import {AppState} from "../../store/app.reducer";
import {IRecipeDetails} from "../../store/recipe.models";
import {
  faHeart,
  faClock,
  faLeaf,
  faCarrot,
  faSeedling,
  faWineBottle,
  faDollarSign
} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

function ViewRecipe(props: any) {
    const details: IRecipeDetails | null = useSelector((state: AppState) => state.recipeDetails.details);
    const dispatch = useDispatch();
    const currentPathSplit = props.location.pathname.split('/');
    const recipeId = currentPathSplit[currentPathSplit.length - 1];
    useEffect(() =>  {
        dispatch(GetRecipeDetailsById(recipeId));
    }, [recipeId, dispatch]);


    return (
        <div className="details-wrapper">
            <div className="details-title" style={{marginLeft: "80px"}}>{details?.title}</div>
            <div className="details-tags">
              {details?.dishTypes.map(type => (
                <label className="recipe-details-label">{type}</label>
              ))}

              {details?.diets.map(diet => (
                <label className="recipe-details-label">{diet}</label>
              ))}
            </div>
            <div className="recipe-main-details">
              <div className="details-description">
                  <div className="main-details-block">
                    <div className="details-icon"><FontAwesomeIcon icon={faHeart}/></div>
                      <p>{details?.aggregateLikes} Likes</p>
                  </div>
                  { details?.readyInMinutes &&
                    <div className="main-details-block">
                        <div className="details-icon"><FontAwesomeIcon icon={faClock}/></div>
                      <p>{details?.readyInMinutes} Minutes</p>
                    </div>
                  }

                  { details?.vegetarian &&
                  <div className="main-details-block">
                      <div className="details-icon"><FontAwesomeIcon icon={faCarrot}/></div>
                      <p>Vegetarian</p>
                  </div>
                  }

                  { details?.healthScore &&
                  <div className="main-details-block">
                      <div className="details-icon"><FontAwesomeIcon icon={faLeaf}/></div>
                      <p>{ details?.healthScore } Points (Health Score)</p>
                  </div>
                  }

                  { details?.pricePerServing &&
                  <div className="main-details-block">
                      <div className="details-icon"><FontAwesomeIcon icon={faDollarSign}/></div>
                      <p>{ details?.pricePerServing } (Price per serving)</p>
                  </div>
                  }

                  { details?.glutenFree &&
                  <div className="main-details-block">
                      <div className="details-icon"><FontAwesomeIcon icon={faSeedling}/></div>
                      <p>Gluten Free</p>
                  </div>
                  }

                  { details?.dairyFree &&
                  <div className="main-details-block">
                      <div className="details-icon"><FontAwesomeIcon icon={faWineBottle}/></div>
                      <p>Dairy Free</p>
                  </div>
                  }

                </div>
                <div className="recipe-image-wrapper">
                    <img className="recipe-image" src={details?.image}/>
                </div>
            </div>
            <div className="details-description-section">
                <p className="details-title">Steps</p>
                {details?.analyzedInstructions.map(instruction => (
                  instruction.steps.map(step => (
                    <div className="cooking-timeline-item">
                      <p className="number">{step.number}</p>
                      <p className="description">{step.step}</p>
                    </div>
                  ))
                ))}
            </div>
            <div className="ingredients-section">
              <p className="details-title">Ingredients</p>
              <div className="ingredients">
                {
                  details?.extendedIngredients.map((ingredient) => (
                    <div className="ingredient">
                      <img src={`https://spoonacular.com/cdn/ingredients_100x100/${ingredient?.image}`} alt={ingredient.name}/>
                      <label>{ingredient.original}</label>
                    </div>
                  ))
                }
              </div>
            </div>
        </div>

    );
}

export default ViewRecipe;
