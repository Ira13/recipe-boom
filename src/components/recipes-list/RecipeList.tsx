import React from "react";
import "./RecipeList.scss";
import { IRecipeListItem } from "../../store/recipe.models";
import { useSelector } from "react-redux";
import { AppState } from "../../store/app.reducer";
import RecipeListItem from "./list-item/RecipeListItem";

function RecipeList() {

	const recipeList: IRecipeListItem[] = useSelector((state: AppState) => state.recipes.list);
	return (
			<div className='recipes-container'>
				{
					recipeList.map((recipe: IRecipeListItem) => {
						return <RecipeListItem key={recipe.id} recipe={recipe}/>;
					})
				}
			</div>
	);
}

export default RecipeList;
