import React from "react";
import { useHistory } from 'react-router-dom';
import {IRecipeListItem} from "../../../store/recipe.models";
import "./RecipeListItem.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLeaf, faThumbsUp } from "@fortawesome/free-solid-svg-icons";

interface IProps {
	recipe: IRecipeListItem,
}

function RecipeListItem(props: IProps) {

	const {recipe} = props;
	const imageBackground ={
		backgroundImage: 'url(' + recipe.image + ')'
	}
	const history = useHistory();

	const handleClick = () => history.push(`view/${recipe.id}`);

	const dishTypes = () => {
		if (recipe.dishTypes) {
			return recipe.dishTypes.map((type, i) => {
					if (i < 2) {
						return <label className='label'>{type}</label>
					}
				})
		}
	}

	return (
			<div className="recipe-box" onClick={handleClick}>
				<div className='image' style={imageBackground}>
					<p className='time'><b>{recipe.readyInMinutes} - {recipe.readyInMinutes + 10}</b> min</p>
				</div>
				<h3 className='title'>{recipe.title}</h3>
				<div className='item-info'>
					<label className='score'>
						<FontAwesomeIcon  className='score-icon' icon={faThumbsUp}/>
						{' ' + recipe.aggregateLikes}
					</label>
					{dishTypes()}
					{!recipe.veryHealthy || <FontAwesomeIcon  color='#00c853' icon={faLeaf}/>}
				</div>
			</div>
	);
}

export default RecipeListItem;
