export interface IAddRecipe {
  backgroundImage: BackgroundImage;
  image: string | ArrayBuffer | null | File;
  ingredients: string;
  instructions: string;
  mask: RecipeCardMask;
  readyInMinutes: number;
  servings: number;
  title: string;
}

export enum BackgroundImage {
  none = "none",
  background1 = "background1",
  background2 = "background2",
}

export enum RecipeCardMask {
  ellipseMask = "ellipseMask",
  diamondMask = "diamondMask",
  heartMask = "heartMask",
  potMask = "potMask",
  fishMask = "fishMask",
}
