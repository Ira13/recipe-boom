import React, {useState} from "react";
import {Button, Dialog, DialogActions, DialogContent, DialogTitle, Snackbar, TextField} from "@material-ui/core";
import MuiAlert from '@material-ui/lab/Alert';
import {useFormik} from "formik";
import {IAddRecipe, BackgroundImage, RecipeCardMask} from "./AddRecipe.interface";
import API from "../../utils/API";

export interface AddRecipeProps {
  open: boolean,
  onClose: () => void,
}

export default function AddRecipe(props: AddRecipeProps) {

  const [showSnackBar, setShowSnackBar] = useState(false);
  const form = useFormik<Partial<IAddRecipe>>({
    initialValues: {
      image: null,
      ingredients: '',
      instructions: '',
      readyInMinutes: 0,
      servings: 0,
      title: '',
    },
    onSubmit: values => {
        const recipe: Partial<IAddRecipe> = {
          ...values,
          backgroundImage: BackgroundImage.none,
          mask: RecipeCardMask.ellipseMask,
        };
        postNewRecipe(recipe);
    },
  });

  const handleClose = () => {
    props.onClose();
  };

  const handleCreate = () => {
    form.handleSubmit();
  };

  const postNewRecipe = (recipe: Partial<IAddRecipe>) => {
    const formData = new FormData();
    for (let key in recipe) {
      if (key === 'image') {
        const file = recipe[key] as File;
        formData.append(key, file, file.name)
      } else {
        // @ts-ignore
        const val = recipe[key] as string;
        formData.append(key, val)
      }
    }
    API.post('/visualizeRecipe', formData).then(
      () => {
        setShowSnackBar(true);
        form.resetForm();
        props.onClose();
      },
    );
  };

  return (
    <div>
      <Snackbar open={showSnackBar} autoHideDuration={6000} onClose={() => setShowSnackBar(false)}>
        <MuiAlert elevation={6} variant="filled" severity="success">
          Recipe is created
        </MuiAlert>
      </Snackbar>
      <Dialog open={props.open} onClose={handleClose}>
        <DialogTitle id="form-dialog-title">Create Recipe</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="title"
            label="Recipe Title"
            type="text"
            onChange={form.handleChange}
            value={form.values.title}
            fullWidth
            variant="outlined"
          />
          <TextField
            multiline
            autoFocus
            margin="dense"
            id="ingredients"
            label="Ingredients"
            type="text"
            onChange={form.handleChange}
            value={form.values.ingredients}
            fullWidth
            variant="outlined"
            rows={4}
          />
          <TextField
            multiline
            autoFocus
            margin="dense"
            id="instructions"
            label="Instructions"
            type="text"
            onChange={form.handleChange}
            value={form.values.instructions}
            fullWidth
            variant="outlined"
            rows={4}
          />
          <TextField
            autoFocus
            margin="dense"
            id="readyInMinutes"
            label="Ready In Minutes"
            type="number"
            onChange={form.handleChange}
            value={form.values.readyInMinutes}
            fullWidth
            variant="outlined"
          />
          <TextField
            autoFocus
            margin="dense"
            id="servings"
            label="Servings"
            type="number"
            onChange={form.handleChange}
            value={form.values.servings}
            fullWidth
            variant="outlined"
          />
          <input
            accept="image/*"
            style={{ display: 'none' }}
            id="image"
            multiple
            type="file"
            onChange={(event) => {
              const value = event?.currentTarget?.files ? event?.currentTarget?.files[0] : null;
              form.setFieldValue("image", value);
            }}
          />
          <label htmlFor="image">
            <Button component="span">
              Upload Image
            </Button>
          </label>
          {
            (form.values.image && form.values.image instanceof File) &&
            <p>
                <span style={{fontWeight: "bold"}}>Selected Image:</span> {form.values.image?.name}
            </p>
          }
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleCreate} color="primary">
            Create
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}
