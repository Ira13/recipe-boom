import {IRecipeDetails} from "../recipe.models";

export const SET_RECIPE_DETAILS = "SET_RECIPE_DETAILS";
export const GET_RECIPE_DETAILS = "GET_RECIPE_DETAILS";
export const SET_RECIPE_DETAILS_LOADING = "SET_RECIPE_DETAILS_LOADING";

export interface SetRecipeDetails {
    type: typeof SET_RECIPE_DETAILS,
    payload: IRecipeDetails,
}

export interface SetRecipeDetailsLoading {
    type: typeof SET_RECIPE_DETAILS_LOADING,
    payload: boolean,
}

export interface GetRecipeDetails {
    type: typeof GET_RECIPE_DETAILS,
    payload: string,
}

export type RecipeDetailsActionTypes = SetRecipeDetails | GetRecipeDetails | SetRecipeDetailsLoading
