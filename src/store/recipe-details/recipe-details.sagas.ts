import {call, put, takeEvery, fork} from "redux-saga/effects";
import {SetRecipeDetails, SetRecipeDetailsLoading} from "./recipe-details.actions";
import {GET_RECIPE_DETAILS, GetRecipeDetails} from "./recipe-details.types";
import API from "../../utils/API";

export function* getRecipeDetails(action: GetRecipeDetails) {
    yield put(SetRecipeDetailsLoading(true));
    console.log('works');
    const recipeDetails = yield call(API.get, `/${action.payload}/information`);
    yield put(SetRecipeDetailsLoading(false));
    yield put(SetRecipeDetails(recipeDetails.data));
}

function* watchGetRecipesRequest() {
    yield takeEvery(GET_RECIPE_DETAILS, getRecipeDetails);
}



export const RecipesDetailsSagas = [
    fork(watchGetRecipesRequest),
];
