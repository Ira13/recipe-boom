import {GET_RECIPE_DETAILS, SET_RECIPE_DETAILS, SET_RECIPE_DETAILS_LOADING} from "./recipe-details.types";
import {IRecipeDetails} from "../recipe.models";

export const SetRecipeDetails = (details: IRecipeDetails) => ({type: SET_RECIPE_DETAILS, payload: details});
export const SetRecipeDetailsLoading = (status: boolean) => ({type: SET_RECIPE_DETAILS_LOADING, payload: status});
export const GetRecipeDetailsById = (id: string) => ({type: GET_RECIPE_DETAILS, payload: id});

