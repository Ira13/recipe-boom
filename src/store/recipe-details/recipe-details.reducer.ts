import {IRecipeDetailsState} from "../recipe.models";
import {RecipeDetailsActionTypes, SET_RECIPE_DETAILS, SET_RECIPE_DETAILS_LOADING} from "./recipe-details.types";

export const recipesDetailsInitialState: IRecipeDetailsState = {
	loading: false,
	details: null
};

export const recipeDetailsReducer = (state = recipesDetailsInitialState, action: RecipeDetailsActionTypes) => {
	switch (action.type) {
		case SET_RECIPE_DETAILS:
			return {
				...state,
				details: action.payload
			};
		case SET_RECIPE_DETAILS_LOADING:
			return {
				...state,
				loading: action.payload
			};
		default:
			return state
	}
};
