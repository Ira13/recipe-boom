import { IRecipeListState } from "../recipe.models";
import {
	RecipeListActionTypes,
	SET_RECIPES_LIST,
	SET_RECIPES_LIST_LOADING_STATUS, SET_RECIPES_LIST_SEARCH_OPTIONS,
	UPDATE_RECIPES_LIST_SEARCH_OPTIONS
} from "./recipe-list.types";

export const recipesListInitialState: IRecipeListState = {
	searchOptions: {
		addRecipeInformation: true,
		number: 30,
	},
	list: [],
	isLoading: false,
};

export const recipeListReducer = (state = recipesListInitialState, action: RecipeListActionTypes) => {
	switch (action.type) {
		case SET_RECIPES_LIST:
			return {
				...state,
				list: action.payload
			};
		case SET_RECIPES_LIST_LOADING_STATUS:
			return {
				...state,
				isLoading: action.payload
			};
		case UPDATE_RECIPES_LIST_SEARCH_OPTIONS:
			return {
				...state,
				searchOptions: {
					...state.searchOptions,
					...action.payload
				}
			};
		case SET_RECIPES_LIST_SEARCH_OPTIONS:
			return {
				...state,
				searchOptions:  {
					...recipesListInitialState.searchOptions,
					...action.payload
				}
			};
		default:
			return state
	}
};
