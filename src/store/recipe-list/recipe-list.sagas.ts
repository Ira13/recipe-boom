import API from "../../utils/API";
import {SetRecipesList, SetRecipesListLoadingStatus} from "./recipe-list.actions";
import {call, fork, put, select, takeEvery} from 'redux-saga/effects'
import {FETCH_RANDOM_RECIPES_LIST, FETCH_RECIPES_LIST} from "./recipe-list.types";
import {AppState} from "../app.reducer";
import {OrdinaryListResponse, RandomListResponse} from "../recipe.models";

export function* fetchRecipes() {
	yield put(SetRecipesListLoadingStatus(true));
	const state: AppState = yield select();
	const params = state.recipes.searchOptions;
	const response: OrdinaryListResponse = yield call(API.get,"/searchComplex", {params});
	yield put(SetRecipesListLoadingStatus(false));
	yield put(SetRecipesList(response.data.results));
};

export function* fetchRandomRecipes() {
	yield put(SetRecipesListLoadingStatus(true));
	const params = {
		number: 30,
	}
	const response: RandomListResponse = yield call(API.get,"/random", {params});
	yield put(SetRecipesListLoadingStatus(false));
	yield put(SetRecipesList(response.data.recipes));
};


function* watchGetRecipesRequest() {
	yield takeEvery(FETCH_RECIPES_LIST, fetchRecipes);
}

function* watchGetRandomRecipesRequest() {
	yield takeEvery(FETCH_RANDOM_RECIPES_LIST, fetchRandomRecipes);
}

export const RecipesListSagas = [
	fork(watchGetRecipesRequest),
	fork(watchGetRandomRecipesRequest),
];
