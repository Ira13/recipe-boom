import { IRecipeListItem, IRecipeSearchOptions } from "../recipe.models";
import {
	SET_RECIPES_LIST,
	FETCH_RECIPES_LIST,
	FETCH_RANDOM_RECIPES_LIST,
	SET_RECIPES_LIST_LOADING_STATUS, SET_RECIPES_LIST_SEARCH_OPTIONS,
	UPDATE_RECIPES_LIST_SEARCH_OPTIONS
} from "./recipe-list.types";

export const FetchRecipesList = () => ({type: FETCH_RECIPES_LIST});
export const FetchRandomList = () => ({type: FETCH_RANDOM_RECIPES_LIST});
export const SetRecipesList = (recipeList: IRecipeListItem[]) => ({type: SET_RECIPES_LIST, payload: recipeList});
export const SetRecipesListLoadingStatus = (status: boolean) => ({type: SET_RECIPES_LIST_LOADING_STATUS, payload: status});
export const UpdateRecipesListSearchOptions = (searchOptions: IRecipeSearchOptions) => ({type: UPDATE_RECIPES_LIST_SEARCH_OPTIONS, payload: searchOptions});
export const SetRecipesListSearchOptions = (searchOptions: IRecipeSearchOptions) => ({type: SET_RECIPES_LIST_SEARCH_OPTIONS, payload: searchOptions});
