import { IRecipeListItem, IRecipeSearchOptions } from "../recipe.models";

export const FETCH_RECIPES_LIST = "FETCH_RECIPES_LIST";
export const FETCH_RANDOM_RECIPES_LIST = "FETCH_RANDOM_RECIPES_LIST";
export const SET_RECIPES_LIST = "SET_RECIPES_LIST";
export const SET_RECIPES_LIST_LOADING_STATUS = "SET_RECIPES_LIST_LOADING_STATUS";
export const UPDATE_RECIPES_LIST_SEARCH_OPTIONS = "UPDATE_RECIPES_LIST_SEARCH_OPTIONS";
export const SET_RECIPES_LIST_SEARCH_OPTIONS = "SET_RECIPES_LIST_SEARCH_OPTIONS";

export interface FetchRecipesAction {
	type: typeof FETCH_RECIPES_LIST,
}

export interface FetchRandomRecipesAction {
	type: typeof FETCH_RANDOM_RECIPES_LIST,
}

export interface SetRecipesAction {
	type: typeof SET_RECIPES_LIST,
	payload: IRecipeListItem[]
}

export interface SetRecipesLoadingStatusAction {
	type: typeof SET_RECIPES_LIST_LOADING_STATUS,
	payload: boolean,
}

export interface UpdateRecipesListSearchOptions {
	type: typeof UPDATE_RECIPES_LIST_SEARCH_OPTIONS,
	payload: IRecipeSearchOptions,
}

export interface SetRecipesListSearchOptions {
	type: typeof SET_RECIPES_LIST_SEARCH_OPTIONS,
	payload: IRecipeSearchOptions,
}


export type RecipeListActionTypes =
	FetchRecipesAction
	| SetRecipesAction
	| SetRecipesLoadingStatusAction
	| UpdateRecipesListSearchOptions
	| SetRecipesListSearchOptions
