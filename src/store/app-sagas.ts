import { all } from 'redux-saga/effects'
import {RecipesListSagas} from "./recipe-list/recipe-list.sagas";
import {RecipesDetailsSagas} from "./recipe-details/recipe-details.sagas";

export default function* rootSaga() {
    yield all([
        ...RecipesListSagas,
        ...RecipesDetailsSagas,
    ])
}
