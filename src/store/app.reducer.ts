import {applyMiddleware, combineReducers, createStore} from "redux";
import {recipeListReducer} from "./recipe-list/recipe-list.reducer";
import createSagaMiddleware from 'redux-saga'
import {composeWithDevTools} from "redux-devtools-extension";
import rootSaga from "./app-sagas";
import {recipeDetailsReducer} from "./recipe-details/recipe-details.reducer";
import {IRecipeDetailsState, IRecipeListState} from "./recipe.models";

const appReducer = combineReducers({
	recipes: recipeListReducer,
	recipeDetails: recipeDetailsReducer,
});

const sagaMiddleware = createSagaMiddleware()

export type AppState = {
	recipes: IRecipeListState,
	recipeDetails: IRecipeDetailsState,
}

export const store = createStore(appReducer, composeWithDevTools(
		applyMiddleware(sagaMiddleware),
	)
);

sagaMiddleware.run(rootSaga);
