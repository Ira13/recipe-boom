export interface IRecipeListItem {
	id: string;
	title: string;
	readyInMinutes: string;
	image: string;
	aggregateLikes: number;
	cheap: boolean;
	cookingMinutes: number;
	cuisines: string[];
	diets: string[];
	dishTypes: string[];
	glutenFree: boolean;
	healthScore: number;
	lowFodmap: boolean;
	occasions: string[];
	vegan: boolean;
	vegetarian: boolean;
	veryHealthy: boolean;
	veryPopular: boolean;
	summary: string;
}

export interface RandomListResponse {
	data: {
		recipes: IRecipeListItem[],
	}
}

export interface OrdinaryListResponse {
	data: {
		results: IRecipeListItem[],
	}
}

export interface IRecipeSearchOptions {
	query?: string;
	diet?: string;
	cuisine?: string;
	addRecipeInformation?: boolean;
	number?: number;
}

export interface SearchRecipeResponse {
	results: IRecipeListItem[];
	baseUri: string;
	offset: number;
	number: number;
	totalResults: number;
}

export interface IRecipeListState {
	list: IRecipeListItem[];
	isLoading: boolean;
	searchOptions: IRecipeSearchOptions;
}

export interface IRecipeDetails extends IRecipeListItem {
	analyzedInstructions: RecipeInstructions[];
	dairyFree: boolean;
	extendedIngredients: RecipeInstructionIngredientExtended[];
	instructions: string;
	pricePerServing: number;
}

export interface RecipeInstructions {
	name: string;
	steps: RecipeInstructionStep[];
}

export interface RecipeInstructionStep {
	number: number;
	equipment: RecipeInstructionEquipment[];
	ingredients: RecipeInstructionIngredient[];
	step: string;
}

export interface RecipeInstructionEquipment {
	id: number;
	image: string;
	localizedName: string;
	name: string;
}

export interface RecipeInstructionIngredient {
	id: number;
	image: string;
	localizedName: string;
	name: string;
}

export interface RecipeInstructionIngredientExtended extends RecipeInstructionIngredient {
	original: string;
	amount: number
}

export interface IRecipeDetailsState {
	details: IRecipeDetails | null,
	loading: false;
}
