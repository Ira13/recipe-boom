import {BrowserRouter, Route, Switch} from "react-router-dom";
import React, {useRef, useState} from "react";
import Home from "../components/home/Home";
import Header from "../components/header/Header";
import ViewRecipe from "../components/view-recipe/ViewRecipe";
import AddRecipe from "../components/add-recipe-component/AddRecipe";


const AppRouter = () => {
	const [addNewRecipeOpened, setAddNewRecipeOpened] = useState(false);

	const handleAddNewRecipe = () => {
		setAddNewRecipeOpened(true);
	}

	const handleAddNewRecipeModalClose = () => {
		setAddNewRecipeOpened(false);
	};

	return (
		<BrowserRouter>
			<div className='main-wrapper'>
					<AddRecipe open={addNewRecipeOpened} onClose={handleAddNewRecipeModalClose}/>
					<Header addNewRecipe={handleAddNewRecipe}/>
					<Switch>
						<Route path="/" component={Home} exact={true}/>
						<Route path="/view/:id" component={ViewRecipe}/>
					</Switch>
			</div>
		</BrowserRouter>
	);
}

export default AppRouter;
