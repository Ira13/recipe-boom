import React from 'react';
import ReactDOM from 'react-dom';
import './styles/index.scss';
import AppRouter from "./routers/AppRouter";
import { store } from "./store/app.reducer";
import { Provider } from "react-redux";

ReactDOM.render(
	<Provider store={store}>
		<AppRouter/>
	</Provider>,
  document.getElementById('root')
);
