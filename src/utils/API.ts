import axios, { AxiosRequestConfig } from "axios";

export const config: AxiosRequestConfig = {
	baseURL: 'https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes',
	headers: {
		"x-rapidapi-host": "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com",
		"x-rapidapi-key": "94cfb3e411mshf6e5a2a57ab87d8p1be9a0jsn8453308f65d3",
	},
};


export default axios.create(config);
